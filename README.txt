This is a node access module which allows fine-grained access to nodes that can
be changed during commenting. This is particularly useful for project issue
nodes where other values are changed during commenting.

By default, no content type will be enabled to support the comment_acl
functionality. There is currently no interface to configure comment_acl.
Enable comment_acl functionality by running the following for relevant
content type:

drush vset comment_acl_TYPE 1



